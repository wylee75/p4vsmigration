﻿#region

using System;
using System.IO;
using System.Text.RegularExpressions;
using log4net;

#endregion

namespace p4vsmigration
{
    /// <summary>
    ///     Perforce P4VS migration tool
    ///     Modifies solution and project files to remove all bindings
    ///     Spark Networks - Won Lee
    /// </summary>
    internal class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof (Program));

        private static void Main(string[] args)
        {
            Log.InfoFormat("Starting program");

            if (string.IsNullOrEmpty(args[0]))
                throw new Exception("Please provide the starting root path as the first argument.");

            var rootPath = args[0];

            Console.WriteLine("Please make sure you have checked out all latest files.");
            Console.Write("Logging in ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("{0}\n\n", @"c:\logfiles\p4vsmigration.txt");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Retrieving all files recursively...\n\n");

            var solutionFiles = Directory.GetFiles(rootPath, "*.sln", SearchOption.AllDirectories);
            File.WriteAllText("solution file paths.txt", string.Join(Environment.NewLine, solutionFiles));
            Console.Write("Found {0} solution files in {1} saved to ", solutionFiles.Length, rootPath);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("{0}\n\n", "\"solution file paths.txt\"");
            Console.ForegroundColor = ConsoleColor.White;
            Log.InfoFormat("Found {0} solution files in {1} saved to {2}", solutionFiles.Length, rootPath,
                "\"solution file paths.txt\"");

            var projectFiles = Directory.GetFiles(rootPath, "*.csproj", SearchOption.AllDirectories);
            File.WriteAllText("project file paths.txt", string.Join(Environment.NewLine, projectFiles));
            Console.Write("Found {0} project files in {1} saved to ", projectFiles.Length, rootPath);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("{0}\n\n", "\"project file paths.txt\"");
            Console.ForegroundColor = ConsoleColor.White;
            Log.InfoFormat("Found {0} project files in {1} saved to {2}", projectFiles.Length, rootPath,
                "\"project file paths.txt\"");

            var sccFiles = Directory.GetFiles(rootPath, "*.scc", SearchOption.AllDirectories);
            File.WriteAllText("scc file paths.txt", string.Join(Environment.NewLine, sccFiles));
            Console.Write("Found {0} scc files in {1} saved to ", sccFiles.Length, rootPath);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("{0}\n\n", "\"scc file paths.txt\"");
            Console.ForegroundColor = ConsoleColor.White;
            Log.InfoFormat("Found {0} project files in {1} saved to {2}", sccFiles.Length, rootPath,
                "\"scc file paths.txt\"");

            Console.WriteLine("Press enter to modify files...");
            Console.ReadLine();

            foreach (var solutionFile in solutionFiles)
            {
                UpdateSolutionFile(solutionFile);
            }

            foreach (var projectFile in projectFiles)
            {
                UpdateProjectFile(projectFile);
            }

            foreach (var sccFile in sccFiles)
            {
                RemoveFile(sccFile);
            }

            Console.WriteLine("Finished modifying files.");
            Console.WriteLine("Please verify the changes and check in.");
            Log.InfoFormat("Ending program");
        }


        //    SccNumberOfProjects = 3
        //    SccProjectName0 = Perforce\u0020Project
        //    SccLocalPath0 = ..
        //    SccProvider0 = MSSCCI:Perforce\u0020SCM
        //    SccProjectFilePathRelativizedFromConnection0 = P4SCC_SAMPLE_PROJECT\\
        //    SccProjectUniqueName1 = P4SCC_SAMPLE_PROJECT.csproj
        //    SccLocalPath1 = ..
        //    SccProjectFilePathRelativizedFromConnection1 = P4SCC_SAMPLE_PROJECT\\
        //    SccProjectUniqueName2 = ..\\Forms_Project\\Forms_Project.csproj
        //    SccLocalPath2 = ..
        //    SccProjectFilePathRelativizedFromConnection2 = Forms_Project\\
        //    EndGlobalSection
        /// 
        ///     Fine all instances of the following section, remove, save
        ///     GlobalSection(SourceCodeControl) = preSolution
        ///     <param name="path"></param>
        private static void UpdateSolutionFile(string path)
        {
            var fileInfo = new FileInfo(path);
            RemoveReadOnly(fileInfo);

            var fileText = File.ReadAllText(path);
            const string pattern = @"(?s)(GlobalSection\(SourceCodeControl\)).*?(EndGlobalSection)";
            var count = (Regex.Matches(fileText, pattern)).Count;
            fileText = Regex.Replace(fileText, pattern, string.Empty);
            fileText = Regex.Replace(fileText, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
            File.WriteAllText(path, fileText);

            Console.Write("Found ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(count);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" instance(s) Updated ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(path + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.White;
            Log.InfoFormat("Found {0} instance(s) Updated {1}", count, path);
        }

        //<SccProjectName>SAK</SccProjectName>
        //<SccLocalPath>SAK</SccLocalPath>
        //<SccAuxPath>SAK</SccAuxPath>
        //<SccProvider>SAK</SccProvider>
        /// <summary>
        ///     Find all instances of the following xml tags, remove text body, save
        /// </summary>
        /// <param name="path"></param>
        private static void UpdateProjectFile(string path)
        {
            var fileInfo = new FileInfo(path);
            RemoveReadOnly(fileInfo);

            var fileText = File.ReadAllText(path);
            const string pattern =
                @"(?s)(<SccProjectName>).*?(</SccProjectName>)|(<SccLocalPath>).*?(</SccLocalPath>)|(<SccProvider>).*?(</SccProvider>)|(<SccAuxPath>).*?(</SccAuxPath>)";
            var count = (Regex.Matches(fileText, pattern)).Count;
            fileText = Regex.Replace(fileText, pattern, string.Empty);
            fileText = Regex.Replace(fileText, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
            File.WriteAllText(path, fileText);

            Console.Write("Found ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(count);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" instance(s) Updated ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(path + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.White;
            Log.InfoFormat("Found {0} instance(s) Updated {1}", count, path);
        }

        /// <summary>
        ///     Delete a file given the path
        /// </summary>
        /// <param name="path"></param>
        private static void RemoveFile(string path)
        {
            var fileInfo = new FileInfo(path);
            RemoveReadOnly(fileInfo);

            File.Delete(path);

            Console.Write("Deleted  ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(path + Environment.NewLine);
            Console.ForegroundColor = ConsoleColor.White;
            Log.InfoFormat("Deleted {0} ", path);
        }

        private static void RemoveReadOnly(FileInfo info)
        {
            if (info.IsReadOnly)
            {
                info.IsReadOnly = false;
            }
        }
    }
}